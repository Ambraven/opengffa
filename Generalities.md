## Table of Content
- [The Stakes](#the-stakes)
- [People and Cultures](#people-and-cultures)
- [A Mystical Power](#a-mystical-power)
- [Economic of the Galaxy](#economic-of-the-galaxy)
- [The Technology](#the-technology)

## The stakes

- the nomads vs sedantaries
	- nomads organized in tribes/small groups of ships of varying size
	- nomads have some fix installation mutualized between tribes
	- most nomads are from a species from one sector of the galaxy. Planet of origin unclear. They have historians studying the question. 
	- one sedantary society is expending via colonization of uninhabited planets. They colonize worlds on the nomad territories without realising it/they don't care because the nomads don't live on thoses planets.
	- there is an anarchist alliance. They are sedantarie and nomad anarchist groups. They occupy their own territories. 
	- anarchist teritorie is small.
	- nomad territorie not that small but slowly getting smaller.
	- The nomads tolerate the anarchist nomads on their territories and even let them use their infrastructure as long as they follow their rules in their teritories. 
	- Nomads are usually welcomed in anachist teritories as long as their play along.
	- some tribes aren't allowed in anarchist teritories and some anarchists group are banned from nomad space. There are some tension, but basically each side understand decentralized authority.
	- sedantaries society other than anarchist however don't like decentralization and view it as a menace.
	- the galactic republic (not based on the us system thank you very much ) isn't looking for conflict but for trade agreement. They cam be quite aggressive in enforcing those agreements.
	- pirates ! Some are anarchists but that's a stereotype that all anarchist are pirates. Most pirates have a loose sense of hierarchy however.
	- there is mytical Tortuga. In reality there are several planet where pirates have established strongholds because wherever you are in the galaxy you need a friendly harbor where chill and maybe hide.
	- pirates are very much not welcomed in nomad space. Though some nomad tribe have done piracy on occasion.
- Robots exploitation
	- Robots are made by industrialized worlds to be slaves
	- some society recognize robots as sientent beings and don't allow robot slavery.
	- robots are alienated by design. Meaning some aren't OK with robots liberation movement and it's fucked up.
	- obviously anarchists factions are the best option for liberated robots but not the only option. 
		- even in anarchists factions robots tends to regroup because they share culture and hardship that organics can not experience.
	- some place allow robot creation even though they forbid robot slavery. Because some robots do want a legacy of sort.
	- robots do die, although they can live a long time. Their consciousness can't be copied and the artificial brain deteriorate over time. They can hybernate for long period of time, preserving their brain from usage deterioration, though other deterioration can still occur during that time. Some robots have been around for millenia using this technique.
- anarchists vs hierarchists
	- hierachist societies feel threaten by the existence of anarchist societies
	- Anarchist want to emancipate people from hierachist society but they can't risk all out wars
	- Anarchist help insurection movment across the galaxy
	- Settling in coming from a hierachist society can be a chalenge

## People and Cultures 

- Space vickings !! (Because every one love vickings)
	- mercantile society
	- origin from inhospitable lands
	- raids to help survival but also settle and colonise, blend in occasionnally with local power.
	- explorers
	- often a group of raiders will find a new world and just spawn a new kingdom there
- A Roman like republic: [The Qinevian Republic](The Qinevian Republic.md)
	- expensionnist
	- "for peace we need to annexe our neighbours"
	- strong cultural influence
	- strong industrialisation
	- proletaria doesn't vote
	- citoyens still want to sell "democratie" to every one with interresting ressources...
	- don't hesitate to destabilize foreign country to serve their interests if annexion isn't an option
* mesopotamian society ?
	* zigourates
	* debt based economics
	* contracting debt to gods to be repaid when economically healthy (assuming never)
	* ???
	* ishtar
	* need more research
* independent planets as independent cities 
	* a venician republic
		* convoyer of goods
		* convenient neutrality
	* Athenian republic ?
- [The Anarchist sector](The Anarchist sector.md)
	- Anarchist worlds are very cosmopolite even see different species still dominate different worlds
	- Different models of anarchist organisations
	- Refugies from all around the galaxy find home in those worlds
* the nomads
	* several cultures sharing a territory
	* cultures of origin not usually nomadic, became nomads after getting to space. (Possible exceptions)
	* generation ships are a common design
	* forced to claim and defend planets because of sedentary culture claiming unclaimed and undenfended planets the nomads used to stop by
	* rotating military outposts
	* advanced space communications
	* industries around asteroids and on several "native" worlds
	* best space survival technology 
	* some nomads are fully space adapted, even modifying their body to accomodate
	* common culture :
		* minimize impact and protect virgin worlds
		* 0G sports
		* the mothership is sacred. Often personifyed.
		* Same general culture about the Power (adapted to the culture of the ship)
		* Power user advisor in the gouverning group of motherships are more common than not. Sometime the leader of a mothership is a power user but it's uncommon and not very popular.
		* power user are autority figures
		* common economic system between motherships
			* debt based economics between motherships
			* money for private party doing business on foreign motherships. (Generally ponctual exchanges)
			* when two or more motherships meet, they exchanges moneys for their people to use on each other ship.
		* common nomad language + motherships dialects. Differentiation limited by very good communication web.

## A Mystical Power

- the will of everything gives a will to the power.. 
- therefore sientent being have more influence on the power
- turns out most sientent being love life, living and be confortable
- conflict might change that though more or less locally 
- powers might not include telekinesis but influencing is definitely a thing. Maybe a bit of mind reading like instant though useful in fight.
- several group with different doctrines of using the power.
	- a group of peace keeper, from a sedentary libertarian republic. Very centralized.
	- a group where the usage of the power is somewhat secondary, their relation to the power is more cultural, and it's considered good practise to discuss the power and the discourse of ancient people about the power.
	- a very decentralized cult focus on helping people by targeting the root of their problem. They don't hesitate to get offensive against oppressors. Their training focus a lot on emotion management. 
	- a group focalized on gaining material power through the use of The Power tm
		- using negative emotion isn't a goal but they just don't care and they often end up using them a lot because of the echo chamber effect.
- echo chamber effect : you influence the power and it influence you in return
- therefore using the power aggressively and with negative emotions mess with your mind, but being too compationate can lead to immobilism.
- it seems the power is mostly usable by biological being. It doesn't mean robot are separated from it, but they usually can't access it, or at least it's harder for them. Few people know it's even possible for a robot to use the power.

## Economic of the Galaxy

All different cultures have different economic systems and moneys. However, people go around the galaxy often enough that it would be incredibly unpractical if there wasn't any common ground. Generaly speaking the galactic society can use their money in their zone of influence. Societies without money use an influente money instead. 

There is however an alternative : A metal unalterable but not much useful otherwise because other metals generally are better suited for most usage. (it is even too fragile for jewelry normally.)

Hyperspace fuel can be extrated from specific stars, but doing so is detrimental to the planets around thoses stars.

## The Technology

Technoly might vary from one society to another, but they are generally on the same efficiency level, given that it is rather stable and that society spy and retro-ingeneer each other's technology. Culture and ressources can and will make some differences.

generally speaking :

- retrofuturist technology
- spaceship are sturdy, reparation demande change of rather big component, rudimentary computers
- prostectics are visible but fully fonctional
- close combat weapons are common, personnal energy shields too
- energy shield fail if fired on too much and don't stop melee weapons
- portable holographique comunication devices availables for most people
- interstellar communication need communication array in space in orbite around the sun
- no delay instant communication for intra stellar system. Only recorded message for inter stellar correspondance.
- space ship need to get away from a planet to get into Hyperspace
- crossing the galaxy takes a week for space ship with good hyperspace engine, half with the best engines
- medical technology can heal most healness and injuries if patient doesn't die too quickly

![cc by sa](cc_by_sa.png)
