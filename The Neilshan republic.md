- lots of robots do most of the work on homeland
- use quinevian language for trade but have their own language (heavily influenced by qinevian culture)
- masters of trade
	- biggest transporters on this side of the galaxy
	- whole economy runs on trade
	- don't have an army, pay mercenaries for protection. (Space vickings? )
	- big space docs for building the best transporters in the galaxy. ![drawing of a common space truck](IMG_20231017_221454.jpg)[Neilshan space truck Rai Yini](Neilshan space truck Rai Yini.md)
	- Allied with every one except when they try to invade them
	- have colonies and trading post wherever possible.
		- not in anarchist or nomad space, though they have trading post inside some nomad motherships.
- every citizen can vote when adult.
	- citizenship can be optained by being born from citizen parents, or bought.
	- xenophobia is big. Being a citizen who bought their citizenship give you citizen writes but not the same social status as born citizens.
	- family is very important for wealth accumulation reasons.
		- monogamy is the norm since wealth is the goal.
		- single parents are however courant (share nothing)
		- polyamory exist, but a family unit can't have more than 2 patterns plus the childs
		- homosexuality and transgenderism isn't an issue. Union contracts decide which party is the head of the family and gets to keep their name. (Generally the wealthiest)
	- colonized people are exploited 
		- workers have to compete with robots for salaries
			- anti-robot sentiment
		- colonized worker have little to no chance to ever buy citizenship. Not by legal means anyway.
			- crime syndicate lawn money to gain citizenship. The debt fall on the children in case of death since citizenship is hereditary. It is not, however, transmitted to life partner.

## The Neilshan home system

the Neilshan home system consist in a blue giant star with five planet orbiting it, one being a gaz giant in the habitable zone of the planet with several moons, one of which is like a giant ocean with volcanic island spread across its surface. This moon his named Neilsha and is the homeworld of the Neilshans.

The cities on Neilsha are build on ancient submerged islands, and extend both above and below the surface. Above, the buildings are separated by water roads and larges promenades. Non aquatic people typically spend their time above water, though they are reasons for them to go deep and it's not that uncommon.

# The Language of Nɛilshan


Natively known as: nɛilsha /nɛilʃa/

_...and he stood holding his hat and turned his wet face to the wind..._  
gezlɔjɛŋ ap vel irvɛmie ɛmumgimaz beaz gezlɔjɛŋ ɛnkanyɛ jimtoli vel shumjɔko zaɔŋili rɛaz  
**Pronunciation**: /gezlɔjɛŋ ap vel irvɛmie ɛmumgimaz beaz gezlɔjɛŋ ɛnkanʎɛ jimtoli vel ʃumjɔko zaɔŋili rɛaz/  
**Nɛilshan word order**: and he his hat holding stood and to the wind his wet face turned

## Spelling & Phonology

**Consonant inventory**: b d f g j k l m n p r s t v w z ŋ ʃ ʎ ʒ ʣ ʤ ʦ ʧ

|↓Manner/Place→|Bilabial|Labiodental|Alveolar|Palato-alveolar|Palatal|Velar|
|---|---|---|---|---|---|---|
|Nasal|m||n|||ŋ|
|Stop|p b||t d|||k g|
|Affricate|||ʦ ʣ|ʧ ʤ|||
|Fricative||f v|s z|ʃ ʒ|||
|Approximant|||||j||
|Trill|||r||||
|Lateral approximant|||l||ʎ||

**Co-articulated phonemes**

|↓Manner/Place→|Labial-velar|
|---|---|
|Approximant|w|

**Vowel inventory**: a e i o u ɔ ɛ

| |Front|Back|
|---|---|---|
|High|i|u|
|High-mid|e|o|
|Low-mid|ɛ|ɔ|
|Low|a||

**Syllable structure**: Custom defined ?  
**Stress pattern**: No stress ?

**Spelling rules**:

|Pronunciation|Spelling|
|---|---|
|ʎ|y|
|ʃ|sh|
|ʤ|dj|

---

## Grammar

**Main word order**: Subject (Prepositional phrase) Object Verb. “Mary opened the door with a key” turns into Mary with a key the door opened.  
**Adjective order**: Adjectives are positioned before the noun.  
**Adposition**: prepositions ?

  

  

## Pronouns

| |Singular|Plural|
|---|---|---|
|**1st person**|qum /qum/<br><br>I, me, mine|de /de/<br><br>we, us, ours|
|**2nd person**|wel /wel/<br><br>you, yours|bun /bun/<br><br>you all, yours (pl)|
|**3rd person**|ap /ap/<br><br>he, him, his, she, her, hers, it, its|ral /ral/<br><br>they, them, theirs|

  

## Possessive determiners

| |1st person|2nd person|3rd person|
|---|---|---|---|
|**Possessive**|ub /ub/<br><br>my, our|von /von/<br><br>your, your (pl)|vel /vel/<br><br>his, her, its, their|

  

## Numbers

Nɛilshan has a base-12 number system:

1 - ɛl  
2 - zovo  
3 - an  
4 - ɛloŋ  
5 - pekɔ  
6 - djiljɔn  
7 - ŋiʧa  
8 - yi  
9 - gɔ  
10 - keshke  
11 - me  
12 - ol  
144 - domu  
1728 - tɛ  

### Derivational morphology

**Adjective → adverb** = Suffix -ej  
**Adjective → noun (the quality of being [adj])** = If ends with vowel: Suffix -m  
Else: Suffix -o  
**Adjective → verb (to make something [adj])** = Prefix be-  
**Noun → adjective (having the quality of [noun])** = Prefix mo-  
**Noun → adjective relating to noun (e.g. economy → economic)** = If ends with vowel: Suffix -k  
Else: Suffix -ɛ  
**Noun to verb** = If ends with vowel: Suffix -n  
Else: Suffix -o  
**Verb → adjective (result of doing [verb])** = If ends with vowel: Suffix -ʃ  
Else: Suffix -i  
**Tending to** = Suffix -eʎ  
**Verb → noun (the act of [verb])** = Suffix -ɛd  
**Verb → noun that verb produces (e.g. know → knowledge)** = Suffix -ol  
**One who [verb]s (e.g. paint → painter)** = Prefix ʎe-  
**Place of (e.g. wine → winery)** = Suffix -aʣ  
**Diminutive** = If ends with vowel: Suffix -s  
Else: Suffix -ɔ  
**Augmentative** = Prefix dɔ-

### [Vocabulary](Neilshan_Dictionary.pdf)
