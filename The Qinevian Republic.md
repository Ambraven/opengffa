- dominant species is human
- other species are systematically discriminated
- no discrimination between human of difference skin color (enough discriminations around already)
- A cast of voters ( larumboselera, singular: rumboselera )
	- non-human voters exist to very rare and always tokenized
- The home planet is favorized
- Late stage capitalism
- Senate (kuseu)
	- Senators (lawekuseu, singular: wekuseu) are necessarily voters
	- A senator can become dictator (danu) in time of crisis. Dictatorship end with the crisis, or if it becomes clear the dictator don't do a good job resolving the crisis. (Though they has to really visibly do a terrible job, because changing dictator is always risky.)
		- So far most dictators have released power in due time
		- A few tried to keep power and were dealt with
		- two were just replaced, one at his own request
- Provinces (laukkü, singular ukkü) with governors (laweukkü, singular weukkü)
- You can buy right of vote, technically, but it's so expensive very few can actually vote
	- Even inside the voter cast, it's not rare than one party of a family don't bother buying right of vote.
	- Can sometime be a sign of abus
- robots (lasemuik, singular: semuik) are expensive, but less so than the right of vote
- Having domestics instead of robots slaves is viewed as a sign of prosperity among the voting cast
- Only robots can be slaves (technically at least)
- great entertainment industry financed by the voting cast
	- export said entertainment
	- censorship by defunding
	- actors, directors, writers aren't necessarily voting cast (those are actually rare)

## The Power in the Republic (Awet)

- Cast of "peace keepers" (laketuera esu, singular: ketuera esu, short: ketuera) in service of the Senate
	- Can't vote
	- Can be military officers but can't lead armies
	- Aren't authorised to used "dark magic" (Usage of the Power that corrupt the user and the Power) but some do it anyway in secret
	- Have the exclusive legal right to use the Power inside the Republic
	- Hunt down users of "dark magic", and generally any resident of the Republic using the Power who are not from the cast
	- Check on governors and army generals to make sure they don't get any ideas...
	- Basically prevent civil wars.
	- Assassination of authority figures is a common modus operandi in case of insurrection
		- Did not help with the anarchist revolution
		- Assassination of republic dictators has happened when they refused to let go extraordinary powers
	- Founded by the republic (the senate decide their budget)
- Awit is the protector of the Republic
- other Power doctrine/religion are discriminate against

## History

The Quinevian Republic began has a rather small system and proceeded to expend by force, conquering its neighbours when they were judged a security threat to the republic.
The republic growth was slow but steady on their side of the core of the galaxy, until it began to be too big to administrate efficiently from the home system. The territory was then divided in provinces, managed by governors appointed by the senate from the Quinevian home system. Governors were de facto citizens, but they were often not from the Quinevian system itself.

The republic's history is full of civil war and political assassination. This allowed one of the latest and farthest province to become [the anarchist sector](The Anarchist Sector.md).

## Language

The Qinevian language is the dominante form of communication throughout the galaxy because of it's export of entertainment, central position in the galaxy, and millennia of colonisation.

### Spelling & Phonology

**Consonant inventory**: b d g k l m n p q r s t v w ŋ

|↓Manner/Place→|Bilabial|Labiodental|Alveolar|Velar|Uvular|
|---|---|---|---|---|---|
|Nasal|m||n|ŋ||
|Stop|b p||t d|k g|q|
|Fricative||v|s|||
|Trill|||r|||
|Lateral approximant|||l|||

**Co-articulated phonemes**

|↓Manner/Place→|Labial-velar|
|---|---|
|Approximant|w|

**Vowel inventory**: a e i o u

  

| |Front|Back|
|---|---|---|
|High|i|u|
|High-mid|e|o|
|Low|a||

**Syllable structure**: Custom defined ?  
**Stress pattern**: Penultimate — stress is on the second last syllable ?

**Sound changes (in order of application)**:

- a → ɑ̃ \ _n
- o → ɔ̃ \ _n

**Spelling rules**:

|Pronunciation|Spelling|
|---|---|
|ŋ|gn|
|k|k|
|aa|ä|
|uu|ü|
|ee|ë|
|ii|ï|
|oo|ö|

### Grammar

**Main word order**: Verb Object (Prepositional phrase) Subject. “Mary opened the door with a key” turns into Opened the door with a key mary.  
**Adjective order**: Adjectives are positioned after the noun.  
**Adposition**: postpositions

#### Pronouns

Masculine and Feminine only apply to voters
##### Masculine

| |Singular|Plural|
|---|---|---|
|**1st person**|qal /qal/ [ˈqɑ̃anl]<br><br>I (masc), me (masc), mine (masc)|ke /ke/<br><br>we (masc), us (masc), ours (masc)|
|**2nd person**|gnen /ŋen/<br><br>you (masc), yours (masc)|wan /wan/ [ˈwɑ̃ann]<br><br>you (masc) all|
|**3rd person**|mu /mu/<br><br>he, him, his, it (masc), its (masc)|ad /ad/ [ˈɑ̃and]<br><br>they (masc), them (masc), theirs (masc)|

##### Feminine

| |Singular|Plural|
|---|---|---|
|**1st person**|ogn /oŋ/ [ˈɔ̃onŋ]<br><br>I (fem), me (fem), mine (fem)|bo /bo/ [ˈbɔ̃on]<br><br>we (fem), us (fem), ours (fem)|
|**2nd person**|te /te/<br><br>you (fem), yours (fem)|pu /pu/<br><br>you (fem) all|
|**3rd person**|og /og/ [ˈɔ̃ong]<br><br>she, her, hers, it (fem), its (fem)|in /in/<br><br>they (fem), them (fem), theirs (fem)|

##### Neuter

| |Singular|Plural|
|---|---|---|
|**1st person**|sak /sak/ [ˈsɑ̃ank]<br><br>I (neut), me (neut), mine (neut)|ol /ol/ [ˈɔ̃onl]<br><br>we (neut), us (neut), ours (neut)|
|**2nd person**|ig /ig/<br><br>you (neut), yours (neut)|nil /nil/<br><br>you (neut) all|
|**3rd person**|as /as/ [ˈɑ̃ans]<br><br>it (neut), its (neut)|ap /ap/ [ˈɑ̃anp]<br><br>they (neut), them (neut), theirs (neut)|

  

#### Possessive determiners

| |1st person|2nd person|3rd person|
|---|---|---|---|
|**Possessive**|le /le/<br><br>my, our|ko /ko/ [ˈkɔ̃on]<br><br>your, your (pl)|li /li/<br><br>his, her, its, their|

  

#### Noun affixes

Masculine and Feminine only apply to voters

|   |   |
|---|---|
|**Masculine**|Suffix -al|
|**Feminine**|Prefix do-|

|   |   |
|---|---|
|**Singular**|No affix  <br>vegemkau /ˌvegemˈkau/ [vegemˈkɑ̃aˌnu]<br><br>dog|
|**Plural**|Prefix la-  <br>lavegemkau /laˌvegemˈkau/ [lɑ̃ˌanveˈgemkɑ̃ˌanu]<br><br>dogs|

  

## Numbers

Qinevian has a base-12 number system:

1 - eptu  
2 - abat  
3 - egn  
4 - uv  
5 - isqi  
6 - laer  
7 - oqud  
8 - valpan  
9 - sem  
10 - vem  
11 - te  
12 - ad  
144 - vanmok  
1728 - dagem  

### Derivational morphology

**Adjective → adverb** = Suffix -ak  
**Adjective → noun (the quality of being [adj])** = Suffix -et  
**Adjective → verb (to make something [adj])** = If ends with vowel: Suffix -p  
Else: Suffix -i  
**Noun → adjective (having the quality of [noun])** = Prefix pe-  
**Noun → adjective relating to noun (e.g. economy → economic)** = Suffix -ar  
**Noun to verb** = If ends with vowel: Suffix -b  
Else: Suffix -a  
**Verb → adjective (result of doing [verb])** = Prefix bu-  
**Tending to** = Prefix we-  
**Verb → noun (the act of [verb])** = Prefix sa-  
**Verb → noun that verb produces (e.g. know → knowledge)** = Suffix -ag  
**One who [verb]s (e.g. paint → painter)** = If ends with vowel: Suffix -q  
Else: Suffix -a  
**Place of (e.g. wine → winery)** = Prefix ba-  
**Diminutive** = Prefix ba-  
**Augmentative** = If ends with vowel: Suffix -b  
Else: Suffix -o  
**Artificial** = Suffix -ik

### [Vocabulary](Qinevian_dictionary.pdf)