The Anarchist is an ancient Qinevian province far from the center of [the Qinevian Republic](The Qinevian Republic.md). At some point, people of this sector revolted, after centuries of exploitation, and rejected all form of hierarchy based government, inspired by a doctrine of the Power radically different of Awet, in which all life is equal and more potent lifeforms have the responsibility to help less potent ones. (a doctrine which is generally about erasing inequalities and environmentalism, though some individual push the idea to extremes)

## Urbanization

Anarchist planet have kept a lot of Qinevian cities and buildings, though a lot of effort have been made to reduce environmental impact. 

At the same time, the Qinevian republic never completely erased local architectures so, even though administrative, industrial and bussyness building all very Qinevian, habitations and local commercial buildings are more varied from one planet to the other.

The anarchists have also developed space station as place to live. The goal being to not disturb planetary environment were it has been hurt by centuries of exploitation by colonising corporations which often belonged to people living in the center of the Republic.

The governor's planet is the biggest planetary urban center of the sector. Effort are made to remove a lot of this urbanisation to let the biosphere regrow. It's the planet size destruction site. Other planet have suffer more in different ways however, when ressources needed to be exploited or extracted. Some planets are now steriles and a few still toxics.

The anarchists are good at making place habitable and growing things in terrible places

## Defence

The anarchists are not conquerors, but their very existence make them a target for foreign hierarchists powers. They have developed a solid spatial defences using Qinevian technology at the start and investing in optimising defensive defences and technology. They are also using the time tested method of assassination of power figures taken from the Qinevian "peace keepers".

The space habitats are not really made for deep space navigation, but they can still move from a planet to another, from one star to the other even if really needed, and have heavy defences. They are actually more defended than planetary cities, because they're more obvious target.

There is no anarchist army, in the traditional sens. The combatant are trained reservists. They take turn watching the borders and they fight in case of armed conflict. They can leave the fight at any time (though leaving in the middle of a fight is... rather bad for one's reputation). There are people in charge of strategy and coordination. Disobeying their demands is not a crime in itself, but disobeying means taking responsibility for the result of the disobedience. All parties are given all the informations relative to the mission and anyone can discuss strategy and tactics during briefing. Combatants have to agree with the plan. Retention of information is a serious offence, or even a crime if it results in deaths. 

## The Power

In the anarchist sector the power is for everyone, but not all people have facilities using it.

Everyone learn the basics of the anarchist doctrine of the power wich are the same for every anarchist worlds. Those basics are the results of centuries of discussions between practissioners of every anarchist planet and are always up for discussion. 

It is generally admitted that the power should be used for the good of the people and of the biosphere. sacrificing one for the other should be avoided whenever possible.

It is also generally admitted that starting a conflict can be justified if it correct an injustice, but one must be sure the conflict won't do more arm than good, and only if no other more peaceful solution is possible.

Punishing is not the way. An anarchist should only seek to ensure the wrong done should not be done again.

Those are broad moral statement applicable not only by users of the Power by design, because everyone in the anarchist sector is taught this. Users of the Power need to follow those rules even more rigorously however. Those are also broad because although they are always discussed, it's mostly the interpretation of those statements which might change over time.

When a person decide to invest themselves more into the ways of the Power, they generally join the local group and learn their ways, then travel to other planet and meet other groups to learn their ways as well. At some point the user will either join a group and this group will benefit from this user experience, or the user will invest themselves in something else. While learning the ways of the power, Users are encouraged to do some other job outside the Users's groups It's considered part of the training.

## Symbols

Anarchists have plenty of symboles, but one pattern is often used with variations :
![an anarchist flag](anar-flag-1.png)
Here is a black hole version for the black op anarchists:
![anarchist black ops symbol](anar-black-ops.png)
The black ops anarchiste in this univers are not like Starfleet section 31.

They are an official group and they do answer for their actions. They are not above assassination, but they don't have the right to kill whoever they want. Basically if it help against oppression, good. If it start a war, bad. Edge cases are examined carefully.

## History

- At some point in the early days of the anarchist sector [the Qinevian Republic](The Qinevian Republic.md) launched a full invasion in the hope of conquering it back. The anarchist weren't ready for war, so they took the difficult decision to relocate most of their infrastructure and industry to the farthest regions, keeping what military they could afford to loose to slow the Republic's Legions. The Anarchists managed to keep their people fed and the war effort going this way, at the cost of huge sacrifice from the people relocating and being relocated.
	- Some Nomad tribes helped the relocation effort, which costed them greetly since some world-ship were lost in the process, destroyed by the Legions. This brought other nomad tribes to enter the war against [the Qinevian Republic](The Qinevian Republic.md) as well.