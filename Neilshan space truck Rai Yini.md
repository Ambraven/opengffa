This ship is the most used transporter cargo in this part of the galaxy.

![[IMG_20231017_221454.jpg]]

The segments are modules. You can put cargo modules, turret module and propulsion modules as you see fit. Each module has it's own designation, and a full set of the same draft has a designation as well. 

The junctions are somewhat flexibles.

A full propulsion module would have four or more propulsors attached to it. Though the propulsors of the head module is generally suffisent for most travel.

Also modules are standardized so you can replace any with a newer model, or with take one on an abandonned ship.