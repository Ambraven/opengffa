# OpenGFFA

## What is OpenGFFA??

OpenGFFA is a proposition for a new galactic space opera setting. Yet another one ? Yes, but this one is meant to be shared and allow anyone to write stories taking place in it (contrary to a very popular Galaxy Far Far Away).

This settings aim to have the same feeling as Star Wars with much more liberty to tell stories in it, without having to deal with legal issue. You can use this settings for any kind of project, even sell story using this setting, given that you mention this project and share your creations too.

The aim here is to create a shared galactic mythology.

## How to use OpenGFFA ? 

You just do. Read the part of the settings you're interrested in and write a story using it. Just mention that you're doing it.

You can add your own element to the setting. Ideally you would document them, either by submitting your additions here or by forking this project for yourself.

## Where to begin ? 

[The generalities page](Generalities.md) tries to give an overall of the settings. 

## How to contribute ?

This project here is mine, so I'll accept additions at my own discretion. You are, again, welcomed to fork the project if you don't agree with what I accept or not. Mostly I'll try to keep things somewhat consistent and keep any kind of hate speech out. (to be clear, hate speech can exists in this galaxy, but it has no place in its description.) This include racisms, sexism, homophobia, transphobia, etc... Don't create racist charicatures or species with gendered essentialism.
